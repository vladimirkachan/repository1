﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Schemas;
using Microsoft.AspNetCore.Mvc;
using IParkingService = CoolParking.BL.Interfaces.IParkingService;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public partial class VehiclesController : ControllerBase
    {
        readonly IParkingService service;

        public VehiclesController(IParkingService service)
        {
            this.service = service;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<VehicleJson>>> GetAsync()
        {
            return Ok(await Task.Run(service.GetVehicles));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<VehicleJson>> GetAsync(string id)
        {
            if (!Regex.IsMatch(id, Settings.IdPattern))
                return BadRequest($"{id} does not match the format of the car number");
            Vehicle result = null;
            await Task.Run(() => Parking.Instance.Vehicles.TryGetValue(id, out result));
            if (result == null)
                return NotFound($"Car with number {id} not found in the parking.");
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync([FromRoute] string id)
        {
            Dictionary<string, Vehicle> vehicles = Parking.Instance.Vehicles;
            if (!Regex.IsMatch(id, Settings.IdPattern))
                return BadRequest($"{id} does not match the format of the car number");
            if (!vehicles.ContainsKey(id))
                return NotFound($"The given vehicleId '{id}' was not present in the Parking");
            decimal balance = vehicles[id].Balance;
            if (balance < 0)
                return StatusCode((int)HttpStatusCode.PaymentRequired,
                                  $"You must pay a debt in the amount of at least {-balance}!");
            await Task.Run(() => service.RemoveVehicle(id));
            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult> PostAsync([FromBody] VehicleJson json)
        {
            Vehicle vehicle;
            try
            {
                vehicle = new Vehicle(json.Id, json.VehicleType, json.Balance);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (service.GetFreePlaces() == 0)
                return Conflict("You try to add a vehicle on full parking.");
            if (Parking.Instance.Vehicles.ContainsKey(vehicle.Id))
                return Conflict($"Vehicle with Id {vehicle.Id} already exists in the parking.");
            await Task.Run(() => service.AddVehicle(vehicle));
            return CreatedAtAction("POST", json);
        }

    }
}
