﻿using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : ControllerBase
    {
        readonly IParkingService service;

        public ParkingController(IParkingService service)
        {
            this.service = service;
        }

        [HttpGet("balance")]
        public async Task<ActionResult<decimal>> GetBalanceAsync()
        {
            return Ok(await Task.Run(service.GetBalance));
        }

        [HttpGet("capacity")]
        public async Task<ActionResult<int>> GetCapacityAsync()
        {
            return Ok(await Task.Run(service.GetCapacity));
        }

        [HttpGet("freePlaces")]
        public async Task<ActionResult<int>> GetFreePlacesAsync()
        {
            return Ok(await Task.Run(service.GetFreePlaces));
        }
    }
}
