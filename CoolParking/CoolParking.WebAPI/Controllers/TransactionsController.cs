﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Schemas;
using Microsoft.AspNetCore.Mvc;
using IParkingService = CoolParking.BL.Interfaces.IParkingService;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public partial class TransactionsController : ControllerBase
    {
        readonly IParkingService service;

        public TransactionsController(IParkingService service)
        {
            this.service = service;
        }

        [HttpGet("all")]
        public async Task<ActionResult<string>> GetAllTransactionsAsync()
        {
            string logPath = Settings.LogPath;
            if (!System.IO.File.Exists(logPath))
                return NotFound($"The file '{logPath}' is not found");
            return await Task.Run(service.ReadFromLog);
        }

        [HttpGet("last")]
        public async Task<ActionResult<IEnumerable<TransactionInfo>>> GetLastTransactionsAsync()
        {
            return Ok(await Task.Run(service.GetLastParkingTransactions));
        }

        [HttpPut("topUpVehicle")]
        public async Task<ActionResult> TopUpVehicleAsync([FromBody] TopUpJson json)
        {
            string id = json.Id;
            decimal sum = json.Sum;
            if (!Regex.IsMatch(id, Settings.IdPattern))
                return BadRequest($"{id} does not match the format of the car number");
            if (sum < 0)
                return BadRequest($"You cannot top up the balance with a negative sum: {sum}");
            if (!Parking.Instance.Vehicles.ContainsKey(id))
                return NotFound($"The parking does not contain a vehicle with id: {id}");
            await Task.Run(() => service.TopUpVehicle(id, sum));
            return Ok();
        }
    }
}
