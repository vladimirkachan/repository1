﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PowerController : ControllerBase
    {
        readonly ParkingService service;

        public PowerController(IParkingService service)
        {
            this.service = service as ParkingService;
        }

        [HttpGet("start")]
        public async Task<ActionResult> StartAsync()
        {
            if (service == null) return NotFound();
            await Task.Run(service.Start);
            return Ok();
        }

        [HttpGet("stop")]
        public async Task<ActionResult> StopAsync()
        {
            if (service == null) return NotFound();
            await Task.Run(service.Stop);
            return Ok();
        }

    }
}
