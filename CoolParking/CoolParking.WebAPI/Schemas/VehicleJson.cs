﻿using CoolParking.BL.Models;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Schemas
{
    public struct VehicleJson
    {
        [JsonProperty("id")] public string Id { get; set; }
        [JsonProperty("vehicleType")] public int VehicleType { get; set; }
        [JsonProperty("balance")] public decimal Balance { get; set; }
        public override string ToString()
        {
            return $"{Id,-16} {(VehicleType)VehicleType,-20} {Balance:C}";
        }
    }
}
