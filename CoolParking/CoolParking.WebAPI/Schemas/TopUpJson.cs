﻿using Newtonsoft.Json;

namespace CoolParking.WebAPI.Schemas
{
    public struct TopUpJson
    {
        [JsonProperty("id")] public string Id { get; set; }
        [JsonProperty("sum")] public decimal Sum { get; set; }
        public override string ToString()
        {
            return $"Id: {Id}, Sum: {Sum}";
        }
    }
}
