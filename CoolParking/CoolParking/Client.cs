﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking
{
    class Client
    {
        readonly IParkingService parkingService;
        readonly ITimerService withdrawTimer;
        readonly ITimerService logTimer;
        readonly ILogService logService;
        readonly Parking parking = Parking.Instance;

        public Client()
        {
            withdrawTimer = new TimerService { Interval = Settings.TransactionPeriod.TotalMilliseconds };
            logTimer = new TimerService { Interval = Settings.LoggingPeriod.TotalMilliseconds };
            logService = new LogService(Environment.CurrentDirectory + @"\Transactions.log");
            parkingService = new ParkingService(withdrawTimer, logTimer, logService);
        }

        public void Start()
        {
            withdrawTimer.Start();
            logTimer.Start();
        }
        public void Stop()
        {
            withdrawTimer.Stop();
            logTimer.Stop();
        }
        public void AddVehicle(VehicleType vehicleType, decimal balance)
        {
            parkingService.AddVehicle(
            new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), vehicleType, balance));
        }
        public void AddVehicle(string id, VehicleType vehicleType, decimal balance)
        {
            parkingService.AddVehicle(new Vehicle(id, vehicleType, balance));
        }
        public void RemoveVehicle(string vehicleId)
        {
            parkingService.RemoveVehicle(vehicleId);
        }
        public void PrintParkingBalance()
        {
            Console.WriteLine($"Текущий баланс Парковки: {parking.Balance}");
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            parkingService.TopUpVehicle(vehicleId, sum);
            Console.WriteLine($"Баланс авто с № {vehicleId} сейчас: {parking.Vehicles[vehicleId].Balance}");
        }
        public void PrintLastBalance()
        {
            Console.WriteLine($"Cумма заработанных денег за текущий период (до записи в лог): {parkingService.GetBalance()}");
        }
        public void PrintCountOfFreePlaces()
        {
            Console.WriteLine($"Количество свободных мест на парковке: {parkingService.GetFreePlaces()} ");
        }
        public void PrintCountOfBusyPlaces()
        {
            Console.WriteLine($"Количество занятых мест на парковке: {parking.Vehicles.Count}");
        }
        public void PrintVehicleList()
        {
            Console.WriteLine("Список Транспортных средств находящихся на Паркинге:");
            foreach(var vehicle in parking.Vehicles.Values) Console.WriteLine(vehicle);
        }
        public void PrintLastTransactions()
        {
            Console.WriteLine("Транзакции Парковки за текущий период (до записи в лог):");
            foreach(var transaction in parkingService.GetLastParkingTransactions())
                Console.WriteLine(transaction);
        }
        public void PrintTransactionHistory()
        {
            Console.WriteLine("История транзакций из файла Transactions.log: ");
            string history = parkingService.ReadFromLog();
            Console.WriteLine(history);
        }
        public void PrintTransactionByVehicle(string vehicleId)
        {
            Console.WriteLine("История транзакций из файла Transactions.log: ");
            string history = parkingService.ReadFromLog();
            var transactions = GetTransactionsFromHistory(history);
            foreach(var transactionInfo in transactions.Where(t => t.VehicleId == vehicleId))
                Console.WriteLine(transactionInfo);
        }
        public void ChangeParkingCapacity(int value)
        {
            if (value < parking.Vehicles.Count)
                throw new ArgumentException("Parking capacity cannot be less than the number of cars");
            Settings.ParkingCapacity = value;
        }
        public void ChangeTransactionPeriod(int sec)
        {
            if (sec < 1)
                throw new ArgumentException("TransactionPeriod cannot be less than 1 sec.");
            Settings.TransactionPeriod = TimeSpan.FromSeconds(sec);
            withdrawTimer.Interval = Settings.TransactionPeriod.TotalMilliseconds;
        }
        public void ChangeLoggingPeriod(int sec)
        {
            if (sec < 5)
                throw new ArgumentException("LoggingPeriod cannot be less than 5 sec.");
            Settings.LoggingPeriod = TimeSpan.FromSeconds(sec);
            logTimer.Interval = Settings.LoggingPeriod.TotalMilliseconds;
        }
        public void ChangePenaltyFactor(decimal factor)
        {
            if (factor < 1.1m)
                throw new ArgumentException("PenaltyFactor cannot be less than 1.1");
            Settings.PenaltyFactor = factor;
        }
        static IEnumerable<TransactionInfo> GetTransactionsFromHistory(string history)
        {
            history = history.Replace("---", "");
            var lines = history.Split('\n', StringSplitOptions.RemoveEmptyEntries);
            var transactions = new List<TransactionInfo>();
            foreach (var line in lines)
                if (TransactionInfo.TryParse(line, out var transactionInfo)) 
                    transactions.Add(transactionInfo);
            return transactions;
        }
    }
}
