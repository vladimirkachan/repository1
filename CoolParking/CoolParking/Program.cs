﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CoolParking.BL.Models;

namespace CoolParking
{
    class Program
    {
        static Client client = new();
        static ConsoleKeyInfo input;
        static bool running = true;

        static void Main(string[] args)
        {
            Console.Title = "CoolParking";
            PrintGlobalVariableAndSettings();
            Thread.Sleep(1000);
            client.AddVehicle(VehicleType.PassengerCar, 50);
            client.AddVehicle(VehicleType.Truck, 100);
            client.AddVehicle(VehicleType.Bus, 12);
            client.AddVehicle(VehicleType.Motorcycle, 33);
            Task.Factory.StartNew(UpdateParkingState);
            while (true)
            {
                input = Console.ReadKey();
                running = false;
                Thread.Sleep(1000);
                Console.Clear();
                switch (input.KeyChar)
                {
                    case '1':
                        client.PrintVehicleList();
                        RemoveVehicle();
                        break;
                    case '2':
                        AddVehicle();
                        break;
                    case '3':
                        client.PrintVehicleList();
                        TopUpVehicle();
                        break;
                    case '4':
                        client.PrintLastTransactions();
                        Console.WriteLine();
                        client.PrintLastBalance();
                        PressAnyKey();
                        break;
                    case '5':
                        client.PrintTransactionHistory();
                        PressAnyKey();
                        break;
                    case '6':
                        client.PrintVehicleList();
                        client.PrintTransactionByVehicle(InputVehicleId());
                        PressAnyKey();
                        break;
                    case '7':
                        ChangeParkingCapacity();
                        break;
                    case '8':
                        ChangeTransactionPeriod();
                        break;
                    case '9':
                        ChangeLoggingPeriod();
                        break;
                    case '0':
                        ChangePenaltyFactor();
                        break;
                    case 'g':
                    case 'G':
                    case 'п':
                    case 'П':
                        PrintGlobalVariableAndSettings();
                        PressAnyKey();
                        break;
                    case 'w':
                    case 'W':
                    case 'ц':
                    case 'Ц':
                        if (!running) client.Start();
                        break;
                    case 's':
                    case 'S':
                    case 'ы':
                    case 'Ы':
                        client.Stop();
                        goto ONCE;
                    case 'q':
                    case 'Q':
                    case 'й':
                    case 'Й':
                        return;
                }
                running = true;
            ONCE:
                Task.Factory.StartNew(UpdateParkingState);
            }
        }
        static void UpdateParkingState()
        {

            do
            {
                Thread.Sleep(1000);
                Console.Clear();
                Console.WriteLine(DateTime.Now);
                client.PrintParkingBalance();
                client.PrintCountOfFreePlaces();
                client.PrintCountOfBusyPlaces();
                client.PrintVehicleList();
                Console.WriteLine("\nВведите цифру:");
                Console.WriteLine("1 - забрать авто");
                Console.WriteLine("2 - поставить авто");
                Console.WriteLine("3 - пополнить баланс авто");
                Console.WriteLine("4 - показать последние транзакции и сумму денег до записи в лог");
                Console.WriteLine("5 - показать историю Transaction.log");
                Console.WriteLine("6 - показать все транзакции по конкретному авто");
                Console.WriteLine("7 - изменить вместимость паркинга");
                Console.WriteLine("8 - изменить период транзакций");
                Console.WriteLine("9 - изменить период записи в лог");
                Console.WriteLine("0 - изменить коэффициент штрафа");
                Console.WriteLine("g - показать глобальные переменные и конфигурации");
                Console.WriteLine("w - старт Парковки");
                Console.WriteLine("s - стоп работы парковки");
                Console.WriteLine("Q - выход из программы");
            }
            while (running);
        }
        static void ChangeParkingCapacity()
        {
            int value = InputInt();
            try
            {
                client.ChangeParkingCapacity(value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        static void ChangeTransactionPeriod()
        {
            int sec = InputInt();
            try
            {
                client.ChangeTransactionPeriod(sec);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        static void ChangeLoggingPeriod()
        {
            int sec = InputInt();
            try
            {
                client.ChangeLoggingPeriod(sec);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        static void ChangePenaltyFactor()
        {
            decimal factor = InputDecimal();
            try
            {
                client.ChangePenaltyFactor(factor);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        static void PressAnyKey()
        {
            Console.WriteLine("\nДля перехода к текущему состоянию нажмите любую клавишу");
            Console.ReadKey();
        }
        static void TopUpVehicle()
        {
            var id = InputVehicleId();
            TopUpConcreteVehicle(id);
        }
        static void TopUpConcreteVehicle(string id)
        {
            decimal sum = InputDecimal();
            try
            {
                client.TopUpVehicle(id, sum);
                Thread.Sleep(1000);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Операция пополнения не удалась!");
                Console.WriteLine(ex.Message);
            }
        }
        static void RemoveVehicle()
        {
            var id = InputVehicleId();
            try
            {
                client.RemoveVehicle(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (Parking.Instance.Vehicles.TryGetValue(id, out var vehicle) && vehicle.Balance < 0)
                {
                    Console.WriteLine($"Необходимо погасить долг. Пополнить баланс на сумму, большую чем {-vehicle.Balance} y.e.");
                    Console.WriteLine("Пополнить сейчас? y / n");
                    if (Console.ReadLine()?.Trim().ToLower() != "n")
                        TopUpConcreteVehicle(vehicle.Id);
                }
            }
        }
        static string InputVehicleId()
        {
            Console.Write("Введите № авто: ");
            return Console.ReadLine()?.Trim();
        }
        static void AddVehicle()
        {
            string id = InputVehicleId();
        InputVehicleType:
            Console.Write("Укажите тип авто цифрой 1 - Легковое, 2 - Грузовое, 3 - Автобус, 4 - Мотоцикл: ");
            string s = Console.ReadLine()?.Trim();
            if (!int.TryParse(s, out var n))
            {
                Console.WriteLine("Для выбора типа авто можно вводить только цифры 1, 2, 3, 4");
                goto InputVehicleType;
            }
            VehicleType vehicleType = (VehicleType)n;
            var sum = InputDecimal();
            try
            {
                client.AddVehicle(id, vehicleType, sum);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Операция не удалась!");
                Console.WriteLine(ex.Message);
            }
        }
        static int InputInt()
        {
        Input:
            Console.Write("Введите сумму: ");
            if (!int.TryParse(Console.ReadLine()?.Trim(), out var sum))
            {
                Console.WriteLine("Неверный ввод. Попробуйте ещё раз!");
                goto Input;
            }
            return sum;
        }
        static decimal InputDecimal()
        {
        Input:
            Console.Write("Введите сумму: ");
            if (!decimal.TryParse(Console.ReadLine()?.Trim(), out var sum))
            {
                Console.WriteLine("Неверный ввод. Попробуйте ещё раз!");
                goto Input;
            }
            return sum;
        }
        static void PrintGlobalVariableAndSettings()
        {
            Console.WriteLine($"Начальный баланс Паркинга = {Settings.InitialParkingBalance}");
            Console.WriteLine($"Вместимость Паркинга: {Settings.ParkingCapacity}");
            Console.WriteLine($"Период списания оплаты: {Settings.TransactionPeriod.TotalSeconds} секунд");
            Console.WriteLine($"Период записи в лог: {Settings.LoggingPeriod.TotalSeconds} секунд");
            Console.WriteLine("Тарифы в зависимости от Транспортного средства:");
            foreach (var (vehicleType, tariff) in Settings.VehicleTariffs)
                Console.Write($"{vehicleType} = {tariff} у.е, ");
            Console.Write("\b\b.");
            Console.WriteLine();
            Console.WriteLine($"Коэффициент штрафа: {Settings.PenaltyFactor}");
        }
    }
}
