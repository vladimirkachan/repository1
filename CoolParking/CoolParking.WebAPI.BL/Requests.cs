﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Schemas;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.BL
{
    public static class Requests
    {
        static readonly HttpClient client = new();
        const string host = @"https://localhost:44314/api/";

        static async Task<(HttpStatusCode, string)> GetEndPoint(string url)
        {
            var response = await client.GetAsync(url);
            return (response.StatusCode, await GetResponseBody(response));
        }
        static async Task<string> GetResponseBody(HttpResponseMessage response)
        {
            return await response.Content.ReadAsStringAsync();
        }
        public static async Task<(HttpStatusCode, string)> GetParkingBalance()
        {
            return await GetEndPoint(host + @"parking/balance");
        }
        public static async Task<(HttpStatusCode, string)> GetCapacity()
        {
            return await GetEndPoint(host + @"parking/capacity");
        }
        public static async Task<(HttpStatusCode, string)> GetFreePlaces()
        {
            return await GetEndPoint(host + @"parking/freePlaces");
        }
        public static async Task<(HttpStatusCode, string)> GetAllTransactions()
        {
            return await GetEndPoint(host + @"transactions/all");
        }
        public static async Task<(HttpStatusCode, IEnumerable<TransactionInfo>)> GetLastTransactions()
        {
            var response = await client.GetAsync(host + @"transactions/last");
            var transactions = JsonConvert.DeserializeObject<IEnumerable<TransactionInfo>>(await GetResponseBody(response));
            return (response.StatusCode, transactions);
        }
        public static async Task<(HttpStatusCode, string)> TopUpVehicle(string id, decimal sum)
        {
            var json = new TopUpJson { Id = id, Sum = sum };
            var content = new StringContent(JsonConvert.SerializeObject(json), Encoding.UTF8, "application/json");
            var response = await client.PutAsync(host + @"transactions/topUpVehicle", content);
            return (response.StatusCode, await GetResponseBody(response));
        }
        public static async Task<(HttpStatusCode, IEnumerable<VehicleJson>)> GetVehicles()
        {
            var response = await client.GetAsync(host + "vehicles");
            var vehicles = JsonConvert.DeserializeObject<IEnumerable<VehicleJson>>(await GetResponseBody(response));
            return (response.StatusCode, vehicles);
        }
        public static async Task<(HttpStatusCode, VehicleJson)> GetVehicle(string id)
        {
            var response = await client.GetAsync(host + $@"vehicles/{id}");
            var vehicleJson = JsonConvert.DeserializeObject<VehicleJson>(await GetResponseBody(response));
            return (response.StatusCode, vehicleJson);
        }
        public static async Task<(HttpStatusCode, string)> DeleteVehicle(string id)
        {
            var response = await client.DeleteAsync(host + $@"vehicles/{id}");
            return (response.StatusCode, await GetResponseBody(response));
        }
        public static async Task<(HttpStatusCode, string)> AddVehicle(string id, int vehicleType, decimal balance)
        {
            var json = new VehicleJson { Id = id, VehicleType = vehicleType, Balance = balance };
            var content = new StringContent(JsonConvert.SerializeObject(json), Encoding.UTF8, "application/json");
            var response = await client.PostAsync(host + "vehicles", content);
            return (response.StatusCode, await GetResponseBody(response));
        }
        public static async Task<(HttpStatusCode, string)> Start()
        {
            return await GetEndPoint(host + @"power/start");
        }
        public static async Task<(HttpStatusCode, string)> Stop()
        {
            return await GetEndPoint(host + @"power/stop");
        }

    }
}
