﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.WebAPI.BL;

namespace CoolParking.ConsoleApp
{
    public class Client
    {
        public async void AddVehicle(string id, VehicleType vehicleType, decimal balance)
        {
            var (status, result) = await Requests.AddVehicle(id, (int)vehicleType, balance);
            if (status != System.Net.HttpStatusCode.OK) Console.WriteLine(result);
        }
        public async void TopUpVehicle(string vehicleId, decimal sum)
        {
            await Requests.TopUpVehicle(vehicleId, sum);
            var (code, vehicle) = await Requests.GetVehicle(vehicleId);
            Console.WriteLine($"Vehicle balance with number {vehicleId} now = {vehicle.Balance}");
        }

    }
}
