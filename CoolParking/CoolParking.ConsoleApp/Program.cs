﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.WebAPI.BL;
using CoolParking.WebAPI.Controllers;
using CoolParking.WebAPI.Schemas;
using Newtonsoft.Json;

namespace CoolParking.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Client client = new();
            client.AddVehicle("AA-1111-AA", VehicleType.Bus, 100);

            Console.ReadKey();
        }
    }
}
