﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoolParking.WF
{
    public partial class TopUpForm : Form
    {
        public TopUpForm()
        {
            InitializeComponent();
        }
        protected override void OnVisibleChanged(EventArgs e)
        {
            if (!Visible) return;
            vehicleId1.Text = "";
            sum1.Value = 100;
        }
    }
}
