﻿    using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CoolParking.BL.Models;
using CoolParking.WebAPI.BL;
    using Microsoft.VisualBasic;
    using Newtonsoft.Json;

namespace CoolParking.WF
{
    public partial class MainForm : Form
    {
        readonly AllTransactionsForm allTransactionsForm = new();
        readonly TopUpForm topUpForm = new();
        readonly AddForm addForm = new();
        readonly DeleteForm deleteForm = new();

        public MainForm()
        {
            InitializeComponent();
            allTransactionsForm.Owner = topUpForm.Owner = addForm.Owner = deleteForm.Owner = this;
        }

        protected override async void OnLoad(EventArgs e)
        {
            for (int i = 0; i < 4;)
                await Requests.AddVehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), ++i, 100);
        }
        private void exit1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private async void start1_Click(object sender, EventArgs e)
        {
            await Requests.Start();
        }

        private async void stop1_Click(object sender, EventArgs e)
        {
            await Requests.Stop();
        }

        private async void capacity1_Click(object sender, EventArgs e)
        {
            var (status, capacity) = await Requests.GetCapacity();
            MessageBox.Show($@"Parking Capacity = {capacity}{Environment.NewLine}Code: {status}", 
                            @"CoolParking Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private async void balance1_Click(object sender, EventArgs e)
        {
            var (status, balance) = await Requests.GetParkingBalance();
            MessageBox.Show($@"Parking Balance = {balance}{Environment.NewLine}Code: {status}",
                            @"CoolParking Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private async void freePlaces1_Click(object sender, EventArgs e)
        {
            var (status, freePlaces) = await Requests.GetFreePlaces();
            MessageBox.Show($@"Free parking places = {freePlaces}{Environment.NewLine}Code: {status}",
                            @"CoolParking Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private async void last1_Click(object sender, EventArgs e)
        {
            var (status, transactions) = await Requests.GetLastTransactions();
            StringBuilder builder = new("Last parking transactions for the current period (before logging): " + Environment.NewLine);
            foreach (var t in transactions) builder.AppendLine(t.ToString());
            builder.AppendLine($@"{Environment.NewLine}Code: {status}");
            MessageBox.Show(builder.ToString(),
                            @"CoolParking Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void all1_Click(object sender, EventArgs e)
        {
            allTransactionsForm.ShowDialog();
        }

        private async void topUpVehicle1_Click(object sender, EventArgs e)
        {
            if (topUpForm.ShowDialog() != DialogResult.OK) return;
            var (status, result) = await Requests.TopUpVehicle(topUpForm.vehicleId1.Text, topUpForm.sum1.Value);
            MessageBox.Show($@"Result = {result}{Environment.NewLine}Code: {status}",
                            @"CoolParking Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private async void post1_Click(object sender, EventArgs e)
        {
            if (addForm.ShowDialog() != DialogResult.OK) return;
            var (status, result) = await Requests.AddVehicle(addForm.vehicleId1.Text,
                                                             addForm.vehicleType1.SelectedIndex + 1,
                                                             addForm.balance1.Value);
            MessageBox.Show($@"Result = {result}{Environment.NewLine}Code: {status}",
                            @"CoolParking Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private async void timer1_Tick(object sender, EventArgs e)
        {
            Text = @"CoolParking " + DateTime.Now;
            var (_, vehicles) = await Requests.GetVehicles();
            var items = listBox1.Items;
            items.Clear();
            foreach (var v in vehicles) items.Add(v);
            var (_, transactions) = await Requests.GetLastTransactions();
            items = listBox2.Items;
            items.Clear();
            foreach (var t in transactions) items.Add(t.ToString());
            var (_, balance) = await Requests.GetParkingBalance();
            label1.Text = $@"Parking Balance: {balance}";
            var (_, freePlaces) = await Requests.GetFreePlaces();
            label2.Text = $@"Free Places: {freePlaces}";
        }

        private async void delete1_Click(object sender, EventArgs e)
        {
            if (deleteForm.ShowDialog() != DialogResult.OK) return;
            var (status, result) = await Requests.DeleteVehicle(deleteForm.vehicleId1.Text);
            MessageBox.Show($@"Result = {result}{Environment.NewLine}Code: {status}",
                            @"CoolParking Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private async void getAll1_Click(object sender, EventArgs e)
        {
            var (status, vehicles) = await Requests.GetVehicles();
            string result = vehicles.Aggregate("", (current, v) => current + (v + Environment.NewLine));
            MessageBox.Show($@"{result}{Environment.NewLine}Code: {status}",
                            @"CoolParking Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private async void get1_Click(object sender, EventArgs e)
        {
            string id = Interaction.InputBox("Vehicle Id: ", "Enter ID");
            var (status, vehicle) = await Requests.GetVehicle(id);
            MessageBox.Show($@"{vehicle}{Environment.NewLine}Code: {status}",
                            @"CoolParking Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
