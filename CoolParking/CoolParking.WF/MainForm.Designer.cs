﻿
namespace CoolParking.WF
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.file1 = new System.Windows.Forms.ToolStripMenuItem();
            this.start1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stop1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exit1 = new System.Windows.Forms.ToolStripMenuItem();
            this.parking1 = new System.Windows.Forms.ToolStripMenuItem();
            this.capacity1 = new System.Windows.Forms.ToolStripMenuItem();
            this.balance1 = new System.Windows.Forms.ToolStripMenuItem();
            this.freePlaces1 = new System.Windows.Forms.ToolStripMenuItem();
            this.transactions1 = new System.Windows.Forms.ToolStripMenuItem();
            this.last1 = new System.Windows.Forms.ToolStripMenuItem();
            this.all1 = new System.Windows.Forms.ToolStripMenuItem();
            this.topUpVehicle1 = new System.Windows.Forms.ToolStripMenuItem();
            this.vehicles1 = new System.Windows.Forms.ToolStripMenuItem();
            this.getAll1 = new System.Windows.Forms.ToolStripMenuItem();
            this.get1 = new System.Windows.Forms.ToolStripMenuItem();
            this.post1 = new System.Windows.Forms.ToolStripMenuItem();
            this.delete1 = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.stop2 = new System.Windows.Forms.Button();
            this.start2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.file1,
            this.parking1,
            this.transactions1,
            this.vehicles1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(701, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // file1
            // 
            this.file1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.start1,
            this.stop1,
            this.toolStripSeparator1,
            this.exit1});
            this.file1.Name = "file1";
            this.file1.Size = new System.Drawing.Size(37, 20);
            this.file1.Text = "&File";
            // 
            // start1
            // 
            this.start1.Name = "start1";
            this.start1.Size = new System.Drawing.Size(135, 22);
            this.start1.Text = "&Start";
            this.start1.Click += new System.EventHandler(this.start1_Click);
            // 
            // stop1
            // 
            this.stop1.Name = "stop1";
            this.stop1.Size = new System.Drawing.Size(135, 22);
            this.stop1.Text = "S&top";
            this.stop1.Click += new System.EventHandler(this.stop1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(132, 6);
            // 
            // exit1
            // 
            this.exit1.Name = "exit1";
            this.exit1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exit1.Size = new System.Drawing.Size(135, 22);
            this.exit1.Text = "E&xit";
            this.exit1.Click += new System.EventHandler(this.exit1_Click);
            // 
            // parking1
            // 
            this.parking1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.capacity1,
            this.balance1,
            this.freePlaces1});
            this.parking1.Name = "parking1";
            this.parking1.Size = new System.Drawing.Size(59, 20);
            this.parking1.Text = "&Parking";
            // 
            // capacity1
            // 
            this.capacity1.Name = "capacity1";
            this.capacity1.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.capacity1.Size = new System.Drawing.Size(151, 22);
            this.capacity1.Text = "&Capacity";
            this.capacity1.Click += new System.EventHandler(this.capacity1_Click);
            // 
            // balance1
            // 
            this.balance1.Name = "balance1";
            this.balance1.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.balance1.Size = new System.Drawing.Size(151, 22);
            this.balance1.Text = "&Balance";
            this.balance1.Click += new System.EventHandler(this.balance1_Click);
            // 
            // freePlaces1
            // 
            this.freePlaces1.Name = "freePlaces1";
            this.freePlaces1.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.freePlaces1.Size = new System.Drawing.Size(151, 22);
            this.freePlaces1.Text = "&Free places";
            this.freePlaces1.Click += new System.EventHandler(this.freePlaces1_Click);
            // 
            // transactions1
            // 
            this.transactions1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.last1,
            this.all1,
            this.topUpVehicle1});
            this.transactions1.Name = "transactions1";
            this.transactions1.Size = new System.Drawing.Size(84, 20);
            this.transactions1.Text = "&Transactions";
            // 
            // last1
            // 
            this.last1.Name = "last1";
            this.last1.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.last1.Size = new System.Drawing.Size(169, 22);
            this.last1.Text = "&Last";
            this.last1.Click += new System.EventHandler(this.last1_Click);
            // 
            // all1
            // 
            this.all1.Name = "all1";
            this.all1.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.all1.Size = new System.Drawing.Size(169, 22);
            this.all1.Text = "&All";
            this.all1.Click += new System.EventHandler(this.all1_Click);
            // 
            // topUpVehicle1
            // 
            this.topUpVehicle1.Name = "topUpVehicle1";
            this.topUpVehicle1.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.topUpVehicle1.Size = new System.Drawing.Size(169, 22);
            this.topUpVehicle1.Text = "&Top up vehicle";
            this.topUpVehicle1.Click += new System.EventHandler(this.topUpVehicle1_Click);
            // 
            // vehicles1
            // 
            this.vehicles1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.getAll1,
            this.get1,
            this.post1,
            this.delete1});
            this.vehicles1.Name = "vehicles1";
            this.vehicles1.Size = new System.Drawing.Size(61, 20);
            this.vehicles1.Text = "&Vehicles";
            // 
            // getAll1
            // 
            this.getAll1.Name = "getAll1";
            this.getAll1.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.getAll1.Size = new System.Drawing.Size(155, 22);
            this.getAll1.Text = "Get &All";
            this.getAll1.Click += new System.EventHandler(this.getAll1_Click);
            // 
            // get1
            // 
            this.get1.Name = "get1";
            this.get1.ShortcutKeys = System.Windows.Forms.Keys.F8;
            this.get1.Size = new System.Drawing.Size(155, 22);
            this.get1.Text = "&Get";
            this.get1.Click += new System.EventHandler(this.get1_Click);
            // 
            // post1
            // 
            this.post1.Name = "post1";
            this.post1.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this.post1.Size = new System.Drawing.Size(155, 22);
            this.post1.Text = "&Add Vehicle";
            this.post1.Click += new System.EventHandler(this.post1_Click);
            // 
            // delete1
            // 
            this.delete1.Name = "delete1";
            this.delete1.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this.delete1.Size = new System.Drawing.Size(155, 22);
            this.delete1.Text = "&Delete";
            this.delete1.Click += new System.EventHandler(this.delete1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listBox1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(301, 355);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Vehicles";
            // 
            // listBox1
            // 
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.IntegralHeight = false;
            this.listBox1.ItemHeight = 15;
            this.listBox1.Location = new System.Drawing.Point(3, 19);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(295, 333);
            this.listBox1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.stop2);
            this.panel1.Controls.Add(this.start2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 379);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(701, 40);
            this.panel1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(173, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Free Places:";
            // 
            // stop2
            // 
            this.stop2.Location = new System.Drawing.Point(385, 7);
            this.stop2.Name = "stop2";
            this.stop2.Size = new System.Drawing.Size(75, 23);
            this.stop2.TabIndex = 3;
            this.stop2.Text = "St&op";
            this.stop2.UseVisualStyleBackColor = true;
            this.stop2.Click += new System.EventHandler(this.stop1_Click);
            // 
            // start2
            // 
            this.start2.Location = new System.Drawing.Point(304, 7);
            this.start2.Name = "start2";
            this.start2.Size = new System.Drawing.Size(75, 23);
            this.start2.TabIndex = 2;
            this.start2.Text = "&Start";
            this.start2.UseVisualStyleBackColor = true;
            this.start2.Click += new System.EventHandler(this.start1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Parking Balance: ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listBox2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox2.Location = new System.Drawing.Point(301, 24);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(400, 355);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Last Transactions";
            // 
            // listBox2
            // 
            this.listBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox2.FormattingEnabled = true;
            this.listBox2.IntegralHeight = false;
            this.listBox2.ItemHeight = 15;
            this.listBox2.Location = new System.Drawing.Point(3, 19);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(394, 333);
            this.listBox2.TabIndex = 0;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 419);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CoolParking";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem file1;
        private System.Windows.Forms.ToolStripMenuItem start1;
        private System.Windows.Forms.ToolStripMenuItem stop1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exit1;
        private System.Windows.Forms.ToolStripMenuItem parking1;
        private System.Windows.Forms.ToolStripMenuItem capacity1;
        private System.Windows.Forms.ToolStripMenuItem balance1;
        private System.Windows.Forms.ToolStripMenuItem freePlaces1;
        private System.Windows.Forms.ToolStripMenuItem transactions1;
        private System.Windows.Forms.ToolStripMenuItem last1;
        private System.Windows.Forms.ToolStripMenuItem all1;
        private System.Windows.Forms.ToolStripMenuItem topUpVehicle1;
        private System.Windows.Forms.ToolStripMenuItem vehicles1;
        private System.Windows.Forms.ToolStripMenuItem getAll1;
        private System.Windows.Forms.ToolStripMenuItem get1;
        private System.Windows.Forms.ToolStripMenuItem post1;
        private System.Windows.Forms.ToolStripMenuItem delete1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button stop2;
        private System.Windows.Forms.Button start2;
        private System.Windows.Forms.Label label1;
    }
}

