﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CoolParking.WebAPI.Schemas;

namespace CoolParking.WF
{
    public partial class AddForm : Form
    {

        public AddForm()
        {
            InitializeComponent();
        }
        protected override void OnVisibleChanged(EventArgs e)
        {
            if (!Visible) return;
            vehicleType1.SelectedIndex = 0;
            vehicleId1.Text = "";
            balance1.Value = 100;
        }
    }
}
