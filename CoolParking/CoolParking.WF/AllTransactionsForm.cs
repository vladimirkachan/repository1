﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CoolParking.WebAPI.BL;

namespace CoolParking.WF
{
    public partial class AllTransactionsForm : Form
    {
        public AllTransactionsForm()
        {
            InitializeComponent();
        }

        protected override async void OnVisibleChanged(EventArgs e)
        {
            if (!Visible) return;
            var (status, transactions) = await Requests.GetAllTransactions();
            label1.Text = $@"Code: {status}";
            textBox1.Text = transactions;
            textBox1.Select(textBox1.TextLength, 0);
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Escape) return;
            DialogResult = DialogResult.Cancel;
            Owner.Activate();
        }

    }
}
