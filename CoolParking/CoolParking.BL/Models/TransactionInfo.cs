﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string VehicleId { get; }
        public decimal Sum { get; }
        public DateTime TransactionTime { get; }

        public TransactionInfo(string vehicleId, decimal sum, DateTime transactionTime)
        {
            VehicleId = vehicleId;
            Sum = sum;
            TransactionTime = transactionTime;
        }

        public override string ToString()
        {
            return $"VehicleId: {VehicleId}, Sum: {Sum} cu, TransactionTime: {TransactionTime}"; 
        }

        public static bool TryParse(string input, out TransactionInfo result)
        {
            result = default;
            var m = Regex.Match(input, Settings.IdPattern);
            if (!m.Success) return false;
            var vehicleId = m.Value;
            m = Regex.Match(input, @"Sum: \w+\s");
            if (!m.Success) return false;
            var s = m.Value;
            s = s.TrimEnd();
            s = s.Replace("Sum: ", "");
            if (!decimal.TryParse(s, out var sum)) return false;
            m = Regex.Match(input, @"TransactionTime: [\d\.:\s]*");
            if (!m.Success) return false;
            s = m.Value;
            s = s.Replace("TransactionTime: ", "");
            s = s.Trim('\r');
            if (!DateTime.TryParse(s, out var transactionTime)) return false;
            result = new TransactionInfo(vehicleId, sum, transactionTime);
            return true;
        }
    }
}
