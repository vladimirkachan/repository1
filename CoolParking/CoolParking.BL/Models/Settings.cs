﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal InitialParkingBalance = 0;
        public static int ParkingCapacity = 10;
        public static TimeSpan TransactionPeriod = TimeSpan.FromSeconds(5);
        public static TimeSpan LoggingPeriod = TimeSpan.FromSeconds(60); 
        public static readonly Dictionary<VehicleType, decimal> VehicleTariffs = new()
            { { VehicleType.PassengerCar, 2 }, { VehicleType.Truck, 5 }, { VehicleType.Bus, 3.5m }, { VehicleType.Motorcycle, 1 } };
        public static decimal PenaltyFactor = 2.5m;
        public static string IdPattern = @"[A-Z]{2}-\d{4}-[A-Z]{2}";
        public static string LogPath {get; set;}
    }
}