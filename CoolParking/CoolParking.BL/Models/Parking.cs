﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Collections.Generic;
using System.Timers;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        #region Singleton

        static readonly Lazy<Parking> instance = new();
        public static Parking Instance => instance.Value;

        #endregion

        public Dictionary<string, Vehicle> Vehicles {get;} = new(Settings.ParkingCapacity);
        public decimal Balance {get; internal set;} = Settings.InitialParkingBalance;
         

    }
}
