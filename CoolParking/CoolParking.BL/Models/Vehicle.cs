﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        static readonly Random random = new();
        public static string GenerateRandomRegistrationPlateNumber()
        {
            static string TwoLetters()
            {
                return $"{(char)random.Next(65, 91)}{(char)random.Next(65, 91)}";
            }

            static string FourDigits()
            {
                string s = random.Next(10000).ToString();
                return new string('0', 4 - s.Length) + s;
            }

            return $"{TwoLetters()}-{FourDigits()}-{TwoLetters()}";
        }

        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, int numberType, decimal balance)
        {
            if (!Regex.IsMatch(id, Settings.IdPattern))
                throw new ArgumentException($"Id does not match format 'WW-DDDD-WW', where 'W' is any Latin letter from A to Z, and 'D' is any number: {id}");
            if (numberType is < 1 or > 4)
                throw new ArgumentException(
                $"Invalid parameter value = {numberType}: numberType can only be an integer from 1 to 4 (1 - PassengerCar, 2 - Truck, 3 - Bus, 4 - Motorcycle)");
            if (balance < 0)
                throw new ArgumentException($"Vehicle cannot be initialized with negative balance: {balance}");
            Id = id;
            VehicleType = (VehicleType)numberType;
            Balance = balance;
        }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (!Regex.IsMatch(id, Settings.IdPattern))
                throw new ArgumentException($"Id does not match format 'WW-DDDD-WW', where 'W' is any Latin letter from A to Z, and 'D' is any number: {id}");
            if (balance < 0)
                throw new ArgumentException($"Vehicle cannot be initialized with negative balance: {balance}");
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public decimal TopUpBalance(decimal value)
        {
            Balance += value;
            return Balance;
        }
        public decimal WithdrawPayment()
        {
            decimal tariff = Settings.VehicleTariffs[VehicleType];
            decimal payment = Balance <= 0 ? Settings.PenaltyFactor * tariff :
                Balance < tariff ? Balance + (tariff - Balance) * Settings.PenaltyFactor : tariff;
            Balance -= payment;
            return payment;
        }
        public override string ToString()
        {
            return $"№: {Id}, {VehicleType,-12}, Баланс = {Balance,4}";
        }
    }
}
