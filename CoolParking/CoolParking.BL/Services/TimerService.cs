﻿// TODO: implement class TimerService from the ITimerService interface.
using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        readonly Timer timer = new();

        public event ElapsedEventHandler Elapsed;

        public TimerService()
        {
            timer.Elapsed += OnElapsed;
        }

        public double Interval
        {
            get => timer.Interval;
            set => timer.Interval = value;
        }
        public void Start()
        {
            timer.Start();
        }
        public void Stop()
        {
            timer.Stop();
        }
        public void Dispose()
        {
            timer.Dispose();
        }
        void OnElapsed(object sender, ElapsedEventArgs e)
        {
            Elapsed?.Invoke(sender, e);
        }
    }
}