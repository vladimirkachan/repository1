﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        Parking parking = Parking.Instance;
        ITimerService withdrawTimer = new TimerService {Interval = Settings.TransactionPeriod.TotalMilliseconds},
                      logTimer = new TimerService {Interval = Settings.LoggingPeriod.TotalMilliseconds};
        ILogService logService = new LogService(Environment.CurrentDirectory + @"\Transactions.log");
         List<TransactionInfo> transactions = new();

         Dictionary<string, Vehicle> Vehicles => parking.Vehicles;

        public ParkingService()
        {
            InitializeStart();
        }

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            this.withdrawTimer = withdrawTimer;
            this.logTimer = logTimer;
            this.logService = logService;
            InitializeStart();
        }
        void InitializeStart()
        {

            withdrawTimer.Elapsed += WithdrawTimer_Elapsed;
            logTimer.Elapsed += LogTimer_Elapsed;
            withdrawTimer.Start();
            logTimer.Start();
        }
        public void Dispose()
        {
            Vehicles.Clear();
            withdrawTimer.Dispose();
            logTimer.Dispose();
        }
        public decimal GetBalance()
        {
            return parking.Balance;
        }
        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }
        public int GetFreePlaces()
        {
            return Settings.ParkingCapacity - Vehicles.Count;
        }
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new(new List<Vehicle>(Vehicles.Values));
        }
        public void AddVehicle(Vehicle vehicle)
        {
            int count = Vehicles.Count;
            if (count == Settings.ParkingCapacity)
                throw new InvalidOperationException($"You try to add a vehicle on full parking: count = {count}");
            if (!Vehicles.TryAdd(vehicle.Id, vehicle))
                throw new ArgumentException($"Vehicle with Id {vehicle.Id} already exists");
        }
        public void RemoveVehicle(string vehicleId)
        {
            if (!Vehicles.ContainsKey(vehicleId))
                throw new ArgumentException($"The given vehicleId '{vehicleId}' was not present in the Parking");
            var balance = Vehicles[vehicleId].Balance;
            if (balance < 0)
                throw new InvalidOperationException($"You try to remove vehicle with a negative balance: {balance}");
            Vehicles.Remove(vehicleId);
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
                throw new ArgumentException($"You cannot top up the balance with a negative sum: {sum}");
            if (!Vehicles.ContainsKey(vehicleId))
                throw new ArgumentException($"The parking does not contain a vehicle with id: {vehicleId}");
            Vehicles[vehicleId].TopUpBalance(sum);
        }
        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactions.ToArray();
        }
        public string ReadFromLog()
        {
            return logService.Read();
        }
        public void Start()
        {
            withdrawTimer.Start();
            logTimer.Start();
        }
        public void Stop()
        {
            withdrawTimer.Stop();
            logTimer.Stop();
        }


        void LogTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                foreach (var transaction in transactions)
                    logService.Write(transaction.ToString());
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                return;
            }

            logService.Write("---");
            transactions.Clear();
        }
        void WithdrawTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            foreach (var (id, vehicle) in Vehicles)
            {
                decimal sum = vehicle.WithdrawPayment();
                parking.Balance += sum;
                transactions.Add(new TransactionInfo(id, sum, DateTime.Now));
            }
        }
    }
}